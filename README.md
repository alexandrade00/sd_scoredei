# SD_scoreDEI

## Description
Simple football application

## How to run
1. install docker
2. install docker-compose
3. open os default shell (mac zhs, linux bash, windows cmd) in "/sd_scoredei" directory
4. execute "docker-compose up" in shell
5. enjoy!

## IMPORTANT
Verify if end of line sequence of docker-compose file is in LF!

### P.S.:
We didn't send WAR file because application is in a docker container.
