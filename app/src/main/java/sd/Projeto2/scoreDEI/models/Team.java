package sd.Projeto2.scoreDEI.models;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = true, unique = true)
    private String teamName;

    @Column(name = "logo", nullable = false)
    private String logoURL;

    @OneToMany(mappedBy = "homeTeam")
    private Set<Game> homeGames;

    @OneToMany(mappedBy = "awayTeam")
    private Set<Game> awayGames;

    @OneToMany(mappedBy = "team")
    private Set<Player> players;

    public Team() {
    }

    public Team(String teamName, String logoURL) {
        super();
        this.teamName = teamName;
        this.logoURL = logoURL;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getLogoURL() {
        return this.logoURL;
    }

    public void setLogoURL(String logo) {
        this.logoURL = logo;
    }

    public Set<Game> getAllGames() {
        return Stream.concat(this.homeGames.stream(), this.awayGames.stream())
                .collect(Collectors.toSet());
    }

    public Set<Player> getPlayers() {
        return this.players;
    }
}
