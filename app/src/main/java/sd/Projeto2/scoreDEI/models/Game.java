package sd.Projeto2.scoreDEI.models;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Game")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "localization", nullable = false)
    private String localization;

    @Column(name = "game_date", nullable = false)
    private Date gameDate;

    @ManyToOne
    @JoinColumn(name = "home_team_id", nullable = false)
    private Team homeTeam;

    @ManyToOne
    @JoinColumn(name = "away_team_id", nullable = false)
    private Team awayTeam;

    @OneToMany(mappedBy = "game")
    private Set<Event> events;

    public Game() {
    }

    public Game(String loc, Date date, Team home, Team away) {
        super();
        this.localization = loc;
        this.gameDate = date;
        this.homeTeam = home;
        this.awayTeam = away;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocalization() {
        return this.localization;
    }

    public void setLocalization(String loc) {
        this.localization = loc;
    }

    public Date getGameDate() {
        return this.gameDate;
    }

    public void setGameDate(Date date) {
        this.gameDate = date;
    }

    public Team getHomeTeam() {
        return this.homeTeam;
    }

    public void setHomeTeam(Team home) {
        this.homeTeam = home;
    }

    public Team getAwayTeam() {
        return this.awayTeam;
    }

    public void setAwayTeam(Team away) {
        this.awayTeam = away;
    }

    public Set<Event> getEvents() {
        return this.events;
    }

    public boolean gameHasEnded() {

        for (Event event : this.events) {
            if (event.getName().equals("End Game"))
                return true;
        }

        return false;
    }

    public int[] getScore() {
        int[] result = new int[2];
        result[0] = 0;
        result[1] = 0;

        for (Event event : this.events) {
            if (event.getName().equals("Goal")) {
                if (event.getPlayer().getTeam().getTeamName().equals(homeTeam.getTeamName()))
                    result[0]++;
                else
                    result[1]++;
            }
        }

        return result;
    }

    public String getScoreString() {
        int[] score = getScore();
        return String.format("%d - %d", score[0], score[1]);
    }

    /*
     * 2 - win
     * 1 - draw
     * 0 - lose
     */
    public int getResultForTeam(Team team) {

        int[] score = getScore();

        if (team.getTeamName().equals(homeTeam.getTeamName())) {
            if (score[0] > score[1])
                return 2;
            else if (score[0] < score[1])
                return 0;
            else
                return 1;
        } else {
            if (score[0] < score[1])
                return 2;
            else if (score[0] > score[1])
                return 0;
            else
                return 1;
        }
    }

    public Map<Player, Integer> getScorers() {

        Map<Player, Integer> playersGoals = new HashMap<>();

        for (Event event : this.events) {
            if (event.getName().equals("Goal"))
                playersGoals.compute(event.getPlayer(), (k, v) -> v == null ? 1 : v + 1);
        }

        return playersGoals;
    }

}
