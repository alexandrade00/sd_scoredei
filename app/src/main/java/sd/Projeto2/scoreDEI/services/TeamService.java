package sd.Projeto2.scoreDEI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.exceptions.ResourceNotFoundException;
import sd.Projeto2.scoreDEI.models.Game;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.repository.EventRepo;
import sd.Projeto2.scoreDEI.repository.GameRepo;
import sd.Projeto2.scoreDEI.repository.PlayerRepo;
import sd.Projeto2.scoreDEI.repository.TeamRepo;

@Service
public class TeamService {

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private GameRepo gameRepo;

    @Autowired
    private EventRepo eventRepo;

    @Autowired
    private PlayerRepo playerRepo;

    public List<Team> getAllTeams() {
        return teamRepo.findAll();
    }

    public Team findTeam(String name) {
        return teamRepo.findByTeamName(name);
    }

    public Team findTeam(long teamId) {
        return teamRepo.findById(teamId).get();
    }

    public Team createTeam(Team team) {
        return teamRepo.save(team);
    }

    public void updateTeam(long teamId, Team teamDetails) throws ResourceNotFoundException {
        Team team = teamRepo.findById(teamId)
                .orElseThrow(() -> new ResourceNotFoundException("Team not found for this id :: " + teamId));

        if (!teamDetails.getTeamName().equals(""))
            team.setTeamName(teamDetails.getTeamName());
        if (!teamDetails.getLogoURL().equals(""))
            team.setLogoURL(teamDetails.getLogoURL());

        teamRepo.save(team);
    }

    public void deleteTeam(long teamId)
            throws ResourceNotFoundException {

        Team team = teamRepo.findById(teamId)
                .orElseThrow(() -> new ResourceNotFoundException("Team not found for this id :: " + teamId));
        
        for (Game game : team.getAllGames()) {
            eventRepo.deleteAll(game.getEvents());
            gameRepo.delete(game);
        }

        playerRepo.deleteAll(team.getPlayers());

        teamRepo.delete(team);
    }
}
