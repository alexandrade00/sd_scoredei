package sd.Projeto2.scoreDEI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.exceptions.ResourceNotFoundException;
import sd.Projeto2.scoreDEI.models.User;
import sd.Projeto2.scoreDEI.repository.UserRepo;

@Service
public class UserService {

    @Autowired
    private UserRepo repository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public User createUser(User user, String adminPass) {
        if (user.getRole().equals("ROLE_ADMIN")) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (encoder.matches(adminPass, "$2a$12$0XtquJe/ZVqEY79/a03FieGKnYI1HMX.FnA2z9LVAwF.1GtHzUhAq")) {
                User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()), user.getRole(),user.getEmail(),user.getPhoneNumber());
                return repository.save(newUser);
            }
        }

        User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()), "ROLE_USER",user.getEmail(),user.getPhoneNumber());
        return repository.save(newUser);
    }

    public List<User> getAllUsers() {
        return repository.findAll();
    }

    public User findUser(String username) {
        return repository.findByUsername(username);
    }

    public void updateUser(long userId, User userDetails) throws ResourceNotFoundException {
        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        
        if (!userDetails.getUsername().equals(""))
            user.setUsername(userDetails.getUsername());
        
        if (!userDetails.getEmail().equals(""))
            user.setEmail(userDetails.getEmail());
        
        if (!userDetails.getPassword().equals(""))
            user.setPassword(passwordEncoder.encode(userDetails.getPassword()));
        
        if (!userDetails.getPhoneNumber().equals(""))
            user.setPhoneNumber(userDetails.getPhoneNumber());

        if (!userDetails.getRole().equals(""))
            user.setRole(userDetails.getRole());
        

        repository.save(user);
    }

    public void deleteUser(long userId)
            throws ResourceNotFoundException {


        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        repository.delete(user);
    }

}
