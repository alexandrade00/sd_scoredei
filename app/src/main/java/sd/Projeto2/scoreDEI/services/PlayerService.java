package sd.Projeto2.scoreDEI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.exceptions.ResourceNotFoundException;
import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.repository.PlayerRepo;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepo playerRepo;

    @Autowired
    private EventService eventService;

    public List<Player> getAllPlayers() {
        return playerRepo.findAll();
    }

    public Player findPlayer(String name) {
        return playerRepo.findByPlayerName(name);
    }

    public Player findPlayer(long id) {
        return playerRepo.findById(id).get();
    }

    public Player createPlayer(Player player) {
        return playerRepo.save(player);
    }

    public void updatePlayer(long playerId, Player playerDetails) throws ResourceNotFoundException {
        Player player = playerRepo.findById(playerId)
                .orElseThrow(() -> new ResourceNotFoundException("Player not found for this id :: " + playerId));

        if (playerDetails.getPlayerName() != null)
            player.setPlayerName(playerDetails.getPlayerName());
        if (playerDetails.getBornDate() != null)
            player.setBornDate(playerDetails.getBornDate());
        if (!playerDetails.getPosition().equals(""))
            player.setPosition(playerDetails.getPosition());
        if (playerDetails.getTeam() != null)
            player.setTeam(playerDetails.getTeam());
        playerRepo.save(player);
    }

    public void deletePlayer(long playerId)
            throws ResourceNotFoundException {
        Player player = playerRepo.findById(playerId)
                .orElseThrow(() -> new ResourceNotFoundException("Player not found for this id :: " + playerId));

        eventService.removePlayer(player.getEvents(), player.getTeam());
        playerRepo.delete(player);
    }
}
