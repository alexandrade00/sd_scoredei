package sd.Projeto2.scoreDEI.services;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.models.Event;
import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.repository.EventRepo;
import sd.Projeto2.scoreDEI.repository.PlayerRepo;

@Service
public class EventService {

    @Autowired
    private EventRepo eventRepo;

    @Autowired
    private PlayerRepo playerRepo;

    public Event createEvent(Event event) {
        return eventRepo.save(event);
    }

    public List<Event> findEvents(long gameId) {
        return eventRepo.findByGameId(gameId);
    }

    public List<Event> findAllEvents() {
        return eventRepo.findAll();
    }

    public void deleteEvents(Set<Event> allEvents) {
        eventRepo.deleteAll(allEvents);
    }

    public void removePlayer(Set<Event> events, Team team) {

        Player player = new Player("deleted", "none", new Date(), team);
        playerRepo.save(player);

        for (Event event : events) {
            event.setPlayer(player);
        }

        eventRepo.saveAll(events);
    }

}
