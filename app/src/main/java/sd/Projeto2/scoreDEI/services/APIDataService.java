package sd.Projeto2.scoreDEI.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.repository.TeamRepo;
import sd.Projeto2.scoreDEI.repository.PlayerRepo;

@Service
public class APIDataService {

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private PlayerRepo playerRepo;

    public void fetchTeams() {

        if (!teamRepo.findAll().isEmpty())
            return;

        List<Team> teams = new ArrayList<Team>();
        List<Player> players = new ArrayList<Player>();

        String host = "https://v3.football.api-sports.io/";
        String x_rapidapi_host = "v3.football.api-sports.io";
        // String x_rapidapi_key = "a1578e59caf62678284301e2e14ab7f9";
        String x_rapidapi_key = "9161961d3739f61fae3bed91acb0d5ab";

        String endpoint = "teams";
        String options = "league=94&season=2021";

        HttpResponse<JsonNode> response;
        try {
            response = Unirest.get(host + endpoint + "?" + options)
                    .header("x-rapidapi-host", x_rapidapi_host)
                    .header("x-rapidapi-key", x_rapidapi_key)
                    .asJson();

            // int results =
            // Integer.parseInt(response.getBody().getObject().get("results").toString());
            JSONArray allTeams = (JSONArray) response.getBody().getObject().get("response");

            endpoint = "players";

            for (int i = 0; i < 4; i++) {
                JSONObject oneTeam = allTeams.getJSONObject(i);
                JSONObject teamInfo = oneTeam.getJSONObject("team");
                int teamID = teamInfo.getInt("id");
                Team team = new Team(teamInfo.getString("name"), teamInfo.getString("logo"));
                teams.add(team);

                options = String.format("season=2021&team=%d&page=1", teamID);

                response = Unirest.get(host + endpoint + "?" + options)
                        .header("x-rapidapi-host", x_rapidapi_host)
                        .header("x-rapidapi-key", x_rapidapi_key)
                        .asJson();

                // int totalPages =
                // response.getBody().getObject().getJSONObject("paging").getInt("total");

                for (int page = 1; page <= 1; page++) {
                    options = String.format("season=2021&team=%d&page=%d", teamID, page);

                    response = Unirest.get(host + endpoint + "?" + options)
                            .header("x-rapidapi-host", x_rapidapi_host)
                            .header("x-rapidapi-key", x_rapidapi_key)
                            .asJson();

                    JSONArray allPlayers = response.getBody().getObject().getJSONArray("response");

                    for (int playerIndex = 0; playerIndex < allPlayers.length(); playerIndex++) {

                        String playerName = allPlayers.getJSONObject(playerIndex).getJSONObject("player")
                                .getString("name");
                        Date bornDate = new SimpleDateFormat("dd-MM-yyyy")
                                .parse(allPlayers.getJSONObject(playerIndex).getJSONObject("player")
                                        .getJSONObject("birth").getString("date"));
                        String position = allPlayers.getJSONObject(playerIndex).getJSONArray("statistics")
                                .getJSONObject(0)
                                .getJSONObject("games")
                                .getString("position");

                        Player newPlayer = new Player(playerName, position, bornDate, team);

                        players.add(newPlayer);
                    }
                }

            }

            teamRepo.saveAll(teams);
            playerRepo.saveAll(players);

        } catch (UnirestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
