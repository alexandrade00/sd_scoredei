package sd.Projeto2.scoreDEI.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.Projeto2.scoreDEI.exceptions.ResourceNotFoundException;
import sd.Projeto2.scoreDEI.misc.Statistics;
import sd.Projeto2.scoreDEI.models.Game;
import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.repository.GameRepo;

@Service
public class GameService {

    @Autowired
    private GameRepo gameRepo;

    @Autowired
    private EventService eventService;

    @Autowired
    private TeamService teamService;

    public List<Game> getAllGames() {
        return gameRepo.findAll();
    }

    public Game findGame(long gameId) {
        return gameRepo.findById(gameId).get();
    }

    public Game createGame(Game game) {
        return gameRepo.save(game);
    }

    public void updateGame(long gameId, Game gameDetails) throws ResourceNotFoundException {
        Game game = gameRepo.findById(gameId)
                .orElseThrow(() -> new ResourceNotFoundException("Game not found for this id :: " + gameId));

        if (!gameDetails.getLocalization().equals(""))
            game.setLocalization(gameDetails.getLocalization());
        if (gameDetails.getGameDate() != null)
            game.setGameDate(gameDetails.getGameDate());
        if (gameDetails.getHomeTeam() != null)
            game.setHomeTeam(gameDetails.getHomeTeam());
        if (gameDetails.getAwayTeam() != null)
            game.setAwayTeam(gameDetails.getAwayTeam());
        gameRepo.save(game);
    }

    public void deleteGame(long gameId)
            throws ResourceNotFoundException {
        Game game = gameRepo.findById(gameId)
                .orElseThrow(() -> new ResourceNotFoundException("Game not found for this id :: " + gameId));

        eventService.deleteEvents(game.getEvents());
        gameRepo.delete(game);
    }

    public List<Statistics> getStatistics() {

        List<Game> allGames = getAllGames();
        Map<Team, Statistics> statistics = new HashMap<>(allGames.size());
        Map<Player, Integer> scorers = new HashMap<>(allGames.size() * 10);

        for (Game game : allGames) {

            if (!game.gameHasEnded())
                continue;

            game.getScorers().forEach((k, v) -> {
                scorers.merge(k, v, Integer::sum);
            });

            Team homeTeam = game.getHomeTeam();
            Team awayTeam = game.getAwayTeam();

            int result = game.getResultForTeam(homeTeam);

            statistics.compute(homeTeam, (k, v) -> {
                if (v == null) {
                    return new Statistics(k, 1, result == 2 ? 1 : 0, result == 0 ? 1 : 0, result == 1 ? 1 : 0,
                            new Player());
                } else {
                    v.incrementGames();
                    switch (result) {
                        case 2:
                            v.incrementWins();
                            break;
                        case 1:
                            v.incrementDraws();
                            break;
                        case 0:
                            v.incrementLosses();
                            break;
                    }
                    return v;
                }
            });

            statistics.compute(awayTeam, (k, v) -> {
                if (v == null) {
                    return new Statistics(k, 1, result == 0 ? 1 : 0, result == 2 ? 1 : 0, result == 1 ? 1 : 0,
                            new Player());
                } else {
                    v.incrementGames();
                    switch (result) {
                        case 2:
                            v.incrementLosses();
                            break;
                        case 1:
                            v.incrementDraws();
                            break;
                        case 0:
                            v.incrementWins();
                            break;
                    }
                    return v;
                }
            });

        }

        List<Team> allTeams = teamService.getAllTeams();

        allTeams.forEach(team -> {
            statistics.computeIfPresent(team, (k, v) -> {
                Entry<Player, Integer> player = scorers.entrySet().stream().filter(entry -> entry.getKey().getTeam().equals(k))
                .max(Entry.comparingByValue()).orElse(null);
                v.setBestScorer(player != null ? player.getKey() : new Player("none", "none", new Date(), team));
                return v;
            });
        });

        return statistics.values().stream().toList();
    }

    // public ArrayList<statistics> getStatistics(){
    // List<Team> teamList = teamServ.getAllTeams();
    // int tam = teamList.size();
    // ArrayList<statistics> results = new ArrayList<statistics>();

    // for(int i = 0; i < tam; i++){
    // results.add(new statistics());
    // results.get(i).setTeamId(i);
    // results.get(i).setNumGames(0);
    // results.get(i).setWins(0);
    // results.get(i).setLosses(0);
    // results.get(i).setDraws(0);
    // }
    // List<Game> gameList = getAllGames();

    // try{
    // for (Game game : gameList) {
    // int score1 = getGameResultForTeam(game.getId(),game.getHomeTeam());
    // int score2 = getGameResultForTeam(game.getId(),game.getAwayTeam());

    // results.get((int) game.getHomeTeam().getId()).setNumGames((results.get((int)
    // game.getHomeTeam().getId()).getNumGames())+1);
    // results.get((int) game.getAwayTeam().getId()).setNumGames((results.get((int)
    // game.getAwayTeam().getId()).getNumGames())+1);

    // if(score1 > score2){
    // results.get((int) game.getHomeTeam().getId()).setWins((results.get((int)
    // game.getHomeTeam().getId()).getWins())+1);
    // results.get((int) game.getAwayTeam().getId()).setLosses((results.get((int)
    // game.getAwayTeam().getId()).getLosses())+1);
    // }
    // else if(score1 < score2){
    // results.get((int) game.getHomeTeam().getId()).setLosses((results.get((int)
    // game.getHomeTeam().getId()).getLosses())+1);
    // results.get((int) game.getAwayTeam().getId()).setWins((results.get((int)
    // game.getAwayTeam().getId()).getWins())+1);
    // }
    // else{
    // results.get((int) game.getHomeTeam().getId()).setDraws((results.get((int)
    // game.getHomeTeam().getId()).getDraws())+1);
    // results.get((int) game.getAwayTeam().getId()).setDraws((results.get((int)
    // game.getAwayTeam().getId()).getDraws())+1);
    // }
    // }
    // }catch(IndexOutOfBoundsException e){
    // return results;
    // }
    // return results;
    // }

    // public String[] getScorers(){

    // int aux = 0;
    // List<Player> players = playerService.getAllPlayers();
    // int tam = teamServ.getAllTeams().size();
    // String bestScorers[] = new String[tam];
    // int[][] goals = new int[players.size()][2];
    // List<Event> events = eventService.findAllEvents();

    // for(int i = 0; i < players.size();i++){
    // goals[i][0] = (int) players.get(i).getId();
    // goals[i][1] = 0;
    // goals[i][1] = (int) players.get(i).getTeam().getId();
    // }

    // for (Event event : events) {
    // if(event.getName().equals("Goal"))
    // goals[(int) event.getPlayer().getId()][1]++;
    // }

    // functions.sortbyColumn(goals,2); //Sort por numero de golos

    // while(aux < tam){ //Procurar o melhor marcador para todas as equipas
    // for(int i = 0; i < players.size();i++){
    // if(aux == goals[i][3]){
    // bestScorers[aux] = players.get(i).getPlayerName();
    // aux++;
    // break;
    // }
    // }
    // }

    // return bestScorers;
    // }
}
