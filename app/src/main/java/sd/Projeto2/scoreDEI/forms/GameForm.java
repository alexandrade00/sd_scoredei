package sd.Projeto2.scoreDEI.forms;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class GameForm {

    private long id;
    private String homeTeam;
    private String awayTeam;
    private String local;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date;

    public GameForm() {
        this.date = new Date();
    }

    public long getId(){
        return this.id;
    }
    public long setId(long id){
        return this.id = id;
    }
    
    public String getHomeTeam() {
        return this.homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return this.awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getLocal() {
        return this.local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
