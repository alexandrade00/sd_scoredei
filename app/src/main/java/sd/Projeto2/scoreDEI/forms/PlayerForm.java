package sd.Projeto2.scoreDEI.forms;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class PlayerForm {
    private long id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date birthTime;
    private String teamName;
    private String position;


    public PlayerForm(){
        
    }
    
    public PlayerForm(String name, Date birthTime, String teamId, String position) {
        this.name = name;
        this.birthTime = birthTime;
        this.teamName = teamId;
        this.position = position;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthTime() {
        return this.birthTime;
    }

    public void setBirthTime(Date birthTime) {
        this.birthTime = birthTime;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String teamId) {
        this.teamName = teamId;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
