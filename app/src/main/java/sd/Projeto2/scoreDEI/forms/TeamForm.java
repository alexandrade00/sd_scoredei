package sd.Projeto2.scoreDEI.forms;

public class TeamForm {

    private String name;
    private String urlPhoto;
    private long id;

    public TeamForm() {
    }

    public TeamForm(String name, String urlPhoto) {
        this.name = name;
        this.urlPhoto = urlPhoto;
    }
    public TeamForm(String name, String urlPhoto, long id) {
        this.name = name;
        this.urlPhoto = urlPhoto;
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPhoto() {
        return this.urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
    

}