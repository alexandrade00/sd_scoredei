package sd.Projeto2.scoreDEI.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import sd.Projeto2.scoreDEI.services.DBUserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .requestCache().disable()
                .authorizeRequests()
                .antMatchers("/registerAssets/**", "/gamesAssets/**", "/eventMakerAssets/**", "/eventsAssets/**",
                        "/gameListAssets/**", "/gameViewAssets/**", "/statisticsAssets/**", "/gamesViewAssets/**",
                        "/createPlayersAssets/**", "/createTeamAssets/**", "/statisticsManagerAssets/**", "/manageUsersAssets/**",
                        "/manageTeamAssets/**", "/managePlayersAssets/**", "/manageGameAssets/**", "/loginAssets/**","/list_teamsAssets/**")
                .permitAll()
                .antMatchers("/signup", "/games", "/games/{gameId:[0-9]+}", "/statisticsManager", "/loginAssets/**",
                        "/teams/{teamId:[0-9]+}/statistics", "/")
                .permitAll()
                .antMatchers("/statistics", "/games/{gameId:[0-9]+}/addEvent").hasAnyRole("USER", "ADMIN")
                .antMatchers("/games/addGame", "/fetchData", "/manageUsers","/manageUsers/delete", "/manageTeam/delete", "/manageGame/delete",
                        "/managePlayers/delete", "/createTeam", "/manageTeam", "/managePlayers",
                        "/manageGame","/createPlayers", "/teams")
                .hasRole("ADMIN")
                .anyRequest().denyAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/games")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/games")
                .permitAll();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new DBUserService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService());
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

}
