package sd.Projeto2.scoreDEI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sd.Projeto2.scoreDEI.services.TeamService;
import sd.Projeto2.scoreDEI.forms.TeamForm;
import sd.Projeto2.scoreDEI.misc.Statistics;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.services.APIDataService;
import sd.Projeto2.scoreDEI.services.GameService;

@Controller
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private GameService gameService;

    @Autowired
    private APIDataService dataService;

    @GetMapping("/teams")
    public String viewHomePage(Model model) {
        model.addAttribute("listTeams", teamService.getAllTeams());
        return "list_teams";
    }

    @GetMapping("/fetchData")
    public String fetchDataFromApi(Model model) {
        dataService.fetchTeams();
        return "redirect:/teams";
    }

    @GetMapping("/teams/{teamId}/statistics")
    public String gameView(Model model, @PathVariable long teamId) {
        Team team = teamService.findTeam(teamId);
        String name = team.getTeamName();
        List<Statistics> teamStatistics = gameService.getStatistics();
        model.addAttribute("numGames", teamStatistics.get((int) teamId).getNumGames());
        model.addAttribute("name", name);
        model.addAttribute("team", team);
        model.addAttribute("numGames", teamStatistics.get((int) teamId).getNumGames());
        model.addAttribute("wins", teamStatistics.get((int) teamId).getWins());
        model.addAttribute("draws", teamStatistics.get((int) teamId).getDraws());
        model.addAttribute("losses", teamStatistics.get((int) teamId).getLosses());
        return "statistics";
    }

    @GetMapping("/createTeam")
    public String createTeams(Model model) {
        model.addAttribute("teamForm", new TeamForm());
        return "createTeam";
    }

    @PostMapping("/createTeam")
    public String createTeams(@ModelAttribute TeamForm teamForm) {
        if (teamForm.getName().isEmpty()) {
            return "redirect:/createTeam?invalidName=true";
        } else if (teamService.findTeam(teamForm.getName()) == null) {
            Team newTeam = new Team(teamForm.getName(), teamForm.getUrlPhoto());
            teamService.createTeam(newTeam);
            return "redirect:/games";
        } else {
            return "redirect:/createTeam?teamAlreadyExists=true";
        }
    }

    @GetMapping("/manageTeam")
    public String manageTeams(Model model) {
        List<Team> allTeams = teamService.getAllTeams();
        model.addAttribute("teams", allTeams);
        model.addAttribute("teamForm", new TeamForm());
        return "manageTeam";
    }

    @PostMapping("/manageTeam")
    public String manageTeams(@ModelAttribute TeamForm teamForm) {
        if (teamForm.getName().isEmpty()) {
            return "redirect:/manageTeam?invalidForm=true";
        } else {
            Team updatedTeam = new Team(teamForm.getName(), teamForm.getUrlPhoto());
            teamService.updateTeam(teamForm.getId(), updatedTeam);

            return "redirect:/games";
        }
    }

    @PostMapping("/manageTeam/delete")
    public String deleteTeam(@RequestParam(value = "teamId", required = false) long teamId) {
        if (teamId == 0) {
            return "redirect:/manageTeam?invalidForm=true";
        } else {
            teamService.deleteTeam(teamId);
            return "redirect:/games";
        }
    }
}
