package sd.Projeto2.scoreDEI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sd.Projeto2.scoreDEI.forms.PlayerForm;
import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.services.PlayerService;
import sd.Projeto2.scoreDEI.services.TeamService;

@Controller
public class PlayerController {

    @Autowired
    private PlayerService playerService;
    @Autowired
    private TeamService teamService;

    @GetMapping("/createPlayers")
    public String createPlayers(Model model) {
        String[] positions = new String[] { "Goalkeeper", "Defender", "Midfielder", "Attacker" };

        model.addAttribute("playerForm", new PlayerForm());
        model.addAttribute("listTeams", teamService.getAllTeams());
        model.addAttribute("listPositions", positions);
        return "createPlayers";
    }

    @PostMapping("/createPlayers")
    public String handleForm(@ModelAttribute PlayerForm playerForm) {

        if (playerForm.getName().isEmpty()
                || playerForm.getPosition().isEmpty()
                || playerForm.getTeamName().isEmpty()
                || playerForm.getBirthTime() == null) {
            return "redirect:/createPlayers?invalidForm=true";
        } else {
            Player newPlayer = new Player(playerForm.getName(), playerForm.getPosition(), playerForm.getBirthTime(),
                    teamService.findTeam(playerForm.getTeamName()));
            playerService.createPlayer(newPlayer);
            return "redirect:games";
        }
    }

    @GetMapping("/managePlayers")
    public String managePlayers(Model model) {
        String[] positions = new String[] { "Goalkeeper", "Defender", "Midfielder", "Attacker" };

        model.addAttribute("playerForm", new PlayerForm());
        model.addAttribute("teams", teamService.getAllTeams());
        model.addAttribute("players", playerService.getAllPlayers());
        model.addAttribute("positions", positions);
        return "managePlayers";
    }

    @PostMapping("/managePlayers")
    public String managePlayers(@ModelAttribute PlayerForm playerForm) {
        if (playerForm.getId() == 0) {
            return "redirect:/managePlayers?invalidForm=true";
        } else {
            Team team = teamService.findTeam(playerForm.getTeamName());
            Player originalPlayer = playerService.findPlayer(playerForm.getId());
            Player updatedPlayer = new Player(playerForm.getName(), playerForm.getPosition(), playerForm.getBirthTime(),
                    team);
            playerService.updatePlayer(originalPlayer.getId(), updatedPlayer);

            return "redirect:games";
        }
    }

    @PostMapping("/managePlayers/delete")
    public String deletePlayer(@RequestParam(value = "playerId", required = false) long playerId) {
        if (playerId == 0) {
            return "redirect:/managePlayers?invalidForm=true";
        } else {
            playerService.deletePlayer(playerId);
            return "redirect:/managePlayers";
        }
    }

}
