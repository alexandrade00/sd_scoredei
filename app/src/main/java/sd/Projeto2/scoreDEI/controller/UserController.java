package sd.Projeto2.scoreDEI.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sd.Projeto2.scoreDEI.models.User;
import sd.Projeto2.scoreDEI.forms.UserForm;
import sd.Projeto2.scoreDEI.services.UserService;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/signup")
    public String addUser(Model model) {
        model.addAttribute("userForm", new User());
        return "register";
    }

    @PostMapping("/signup")
    public String handleFormSignup(@ModelAttribute User userForm, @RequestParam("adminPass") String adminPass) {

        if (userForm.getUsername().isEmpty()
                || userForm.getEmail().isEmpty()
                || userForm.getPassword().isEmpty()
                || userForm.getPhoneNumber().isEmpty()
                || userForm.getRole().isEmpty()) {
            return "redirect:/signup?invalidForm=true";
        } else if (userService.findUser(userForm.getUsername()) == null) {
            userService.createUser(userForm, adminPass);
            return "redirect:/login";
        } else {
            return "redirect:/signup?userAlreadyExists=true";
        }
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/adminPanel")
    public String adminPanel() {
        return "adminPanel";
    }

    @GetMapping("/adminMainPage")
    public String adminMainPage() {
        return "adminMainPage";
    }

    @GetMapping("/userMainPage")
    public String userMainPage() {
        return "userMainPage";
    }

    @GetMapping("/manageUsers")
    public String manageUsers(Model model) {
        List<User> allUsers = userService.getAllUsers();
        model.addAttribute("users", allUsers);
        model.addAttribute("userForm", new UserForm());
        return "manageUsers";
    }

    @PostMapping("/manageUsers")
    public String manageUsers(@ModelAttribute UserForm userForm) {

        if (userForm.getId() == 0) {
            return "redirect:/manageUsers?invalidForm=true";
        } else {
            User updatedUser = new User(userForm.getUsername(), userForm.getPassword(), userForm.getRole(),
                    userForm.getEmail(), userForm.getPhoneNumber());
            userService.updateUser(userForm.getId(), updatedUser);

            return "redirect:games";
        }
    }

    @PostMapping("/manageUsers/delete")
    public String deleteUsers(@RequestParam(value = "userId", required = false) long userId) {
        if (userId == 0) {
            return "redirect:/manageUsers?invalidForm=true";
        } else {
            userService.deleteUser(userId);
            return "redirect:/games";
        }
    }

}
