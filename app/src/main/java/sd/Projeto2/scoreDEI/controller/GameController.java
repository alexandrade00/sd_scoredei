package sd.Projeto2.scoreDEI.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sd.Projeto2.scoreDEI.models.Game;
import sd.Projeto2.scoreDEI.models.Team;
import sd.Projeto2.scoreDEI.forms.GameForm;
import sd.Projeto2.scoreDEI.models.Event;
import sd.Projeto2.scoreDEI.services.GameService;
import sd.Projeto2.scoreDEI.services.TeamService;

@Controller
public class GameController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private GameService gameService;

    @GetMapping("/")
    public String redirect() {
        return "redirect:/games";
    }

    @GetMapping("/games/addGame")
    public String addGame(Model model) {
        model.addAttribute("gameForm", new GameForm());
        model.addAttribute("listTeams", teamService.getAllTeams());
        return "addGame";
    }

    @PostMapping("/games/addGame")
    public String handleForm(@ModelAttribute GameForm gameForm) {

        if (gameForm.getHomeTeam().isEmpty()
                || gameForm.getAwayTeam().isEmpty()
                || gameForm.getLocal().isEmpty()
                || gameForm.getDate() == null) {
            return "redirect:/games/addGame?invalidForm=true";
        } else if (gameForm.getHomeTeam().equals(gameForm.getAwayTeam())) {
            return "redirect:/games/addGame?equalTeams=true";
        } else {
            Game newGame = new Game(gameForm.getLocal(), gameForm.getDate(),
                    teamService.findTeam(gameForm.getHomeTeam()),
                    teamService.findTeam(gameForm.getAwayTeam()));

            gameService.createGame(newGame);
            return "redirect:/games";
        }
    }

    @GetMapping("/gameView")
    public String gameView() {
        return "gameView";
    }

    @GetMapping("/gameList")
    public String gameList() {
        return "gameList";
    }

    @GetMapping("/searchGame")
    public String searchGame() {
        return "searchGame";
    }

    @GetMapping("/games")
    public String showGames(Model model) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        model.addAttribute("fmt", fmt);
        model.addAttribute("currentDate", new Date());
        model.addAttribute("selectedDate", new Date());
        model.addAttribute("gameService", gameService);
        model.addAttribute("gameForm", new GameForm());
        model.addAttribute("listTeams", teamService.getAllTeams());
        model.addAttribute("listGames", gameService.getAllGames());
        return "gamesView";
    }

    @PostMapping("/games/edit")
    public String editGame(@ModelAttribute GameForm gameForm, @ModelAttribute long gameId) {

        Game updatedGame = new Game(gameForm.getLocal(), gameForm.getDate(),
                teamService.findTeam(gameForm.getHomeTeam()),
                teamService.findTeam(gameForm.getAwayTeam()));
        gameService.updateGame(gameId, updatedGame);

        return "redirect:games";
    }

    @GetMapping("/manageGame")
    public String manageGame(Model model) {
        List<Game> games = gameService.getAllGames();
        List<Team> teams = teamService.getAllTeams();
        model.addAttribute("games", games);
        model.addAttribute("teams", teams);
        model.addAttribute("gameForm", new GameForm());
        return "manageGame";
    }

    @PostMapping("/manageGame/delete")
    public String deleteGame(@RequestParam(value = "gameId", required = false) long gameId) {
        if (gameId == 0) {
            return "redirect:/manageGame?invalidForm=true";
        } else {
            gameService.deleteGame(gameId);
            return "redirect:/games";
        }
    }

    @PostMapping("/manageGame")
    public String manageTeams(@ModelAttribute GameForm gameForm) {
        if (gameForm.getId() == 0) {
            return "redirect:/manageGame?invalidForm=true";
        } else {
            Team homeTeam = teamService.findTeam(gameForm.getHomeTeam());
            Team awayTeam = teamService.findTeam(gameForm.getAwayTeam());
            Game updatedGame = new Game(gameForm.getLocal(), gameForm.getDate(), homeTeam, awayTeam);
            gameService.updateGame(gameForm.getId(), updatedGame);
            return "redirect:/games";
        }
    }

    @GetMapping("/games/{gameId}")
    public String gameView(Model model, @ModelAttribute GameForm gameForm, @PathVariable long gameId) {

        Game thisGame = gameService.findGame(gameId);
        List<Event> sortedEvents = new ArrayList<Event>(thisGame.getEvents());
        Collections.sort(sortedEvents, Collections.reverseOrder());
        model.addAttribute("gameEnded", thisGame.gameHasEnded());
        model.addAttribute("game", thisGame);
        model.addAttribute("gameService", gameService);
        model.addAttribute("sortedEvents", sortedEvents);
        return "gameView";
    }

}
