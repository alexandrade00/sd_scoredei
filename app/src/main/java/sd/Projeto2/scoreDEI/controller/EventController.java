package sd.Projeto2.scoreDEI.controller;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import sd.Projeto2.scoreDEI.forms.GameForm;
import sd.Projeto2.scoreDEI.models.Event;
import sd.Projeto2.scoreDEI.models.Game;
import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.services.EventService;
import sd.Projeto2.scoreDEI.services.GameService;
import sd.Projeto2.scoreDEI.services.PlayerService;

@Controller
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    /*
     * @GetMapping("/games/{gameId}/events")
     * public String events(Model model, @PathVariable long gameId) {
     * 
     * Game thisGame = gameService.findGame(gameId);
     * Set<Event> events = thisGame.getEvents();
     * 
     * model.addAttribute("gameId", gameId);
     * model.addAttribute("events", events);
     * 
     * return "events";
     * }
     */

    // @GetMapping("/eventMaker")
    // public String eventMaker() {
    // return "eventMaker";
    // }

    // @GetMapping("/events")
    // public String events() {
    // return "events";
    // }

    // @GetMapping("/eventChoice")
    // public String eventChoice() {
    // return "eventChoice";
    // }

    @GetMapping("/manageEvent")
    public String manageEvents() {
        return "manageEvent";
    }

    @GetMapping("/games/{gameId}/addEvent")
    public String addEvent(Model model, @PathVariable long gameId) {

        Game thisGame = gameService.findGame(gameId);
        Set<Player> homePlayers = thisGame.getHomeTeam().getPlayers();
        Set<Player> awayPlayers = thisGame.getAwayTeam().getPlayers();
        Set<Player> allPlayers = Stream.concat(homePlayers.stream(), awayPlayers.stream())
                .collect(Collectors.toSet());

        model.addAttribute("listPlayers", allPlayers);
        model.addAttribute("dataForm", new GameForm());
        model.addAttribute("gameId", gameId);

        return "addEvent";
    }

    @PostMapping("/games/{gameId}/addEvent")
    public String handleForm(@ModelAttribute GameForm gameForm, @PathVariable long gameId) {

        Game thisGame = gameService.findGame(gameId);
        long playerId;
        Event newEvent;

        if (gameForm.getHomeTeam().isEmpty()) {
            return "redirect:/games/{gameId}/addEvent?emptyForm=true";
        }

        try {
            playerId = Long.parseLong(gameForm.getAwayTeam());

            if (gameForm.getHomeTeam().equals("Start Game")
                    || gameForm.getHomeTeam().equals("End Game")
                    || gameForm.getHomeTeam().equals("Resume Game")
                    || gameForm.getHomeTeam().equals("Stop Game")) {
                return "redirect:/games/{gameId}/addEvent?noPlayer=true";
            }

            Player player = playerService.findPlayer(playerId);
            newEvent = new Event(gameForm.getHomeTeam(), thisGame, player);
        } catch (NumberFormatException nfe) {
            if (gameForm.getHomeTeam().equals("Goal")
                    || gameForm.getHomeTeam().equals("Yellow Card")
                    || gameForm.getHomeTeam().equals("Red Card")) {
                return "redirect:/games/{gameId}/addEvent?invalidForm=true";
            } else {
                newEvent = new Event(gameForm.getHomeTeam(), thisGame);
            }
        }

        eventService.createEvent(newEvent);
        return "redirect:/games/{gameId}";
    }

}
