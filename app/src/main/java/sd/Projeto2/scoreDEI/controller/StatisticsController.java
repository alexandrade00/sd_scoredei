package sd.Projeto2.scoreDEI.controller;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import sd.Projeto2.scoreDEI.services.GameService;
import sd.Projeto2.scoreDEI.misc.Statistics;

@Controller
public class StatisticsController {

    @Autowired
    private GameService gameService;

    @GetMapping("/statistics")
    public String statistics() {
        return "statistics";
    }

    @GetMapping("/statisticsManager")
    public String statisticsManager(Model model) {
        List<Statistics> teamStatistics = gameService.getStatistics();
        List<Statistics> sortedStatistics = teamStatistics.stream()
                .sorted(Comparator
                        .comparing(Statistics::getWins)
                        .reversed()
                        .thenComparing(stat -> stat.getTeam().getTeamName()))
                .toList();
        model.addAttribute("teamStatistics", sortedStatistics);
        return "statisticsManager";
    }

}
