package sd.Projeto2.scoreDEI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sd.Projeto2.scoreDEI.models.Player;

@Repository
public interface PlayerRepo extends JpaRepository<Player, Long> {
    Player findByPlayerName(String playerName);
}
