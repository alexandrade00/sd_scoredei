package sd.Projeto2.scoreDEI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sd.Projeto2.scoreDEI.models.Event;

@Repository
public interface EventRepo extends JpaRepository<Event, Long> {
    List<Event> findByGameId(long gameId);
}
