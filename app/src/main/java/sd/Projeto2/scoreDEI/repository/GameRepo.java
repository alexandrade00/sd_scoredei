package sd.Projeto2.scoreDEI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sd.Projeto2.scoreDEI.models.Game;

@Repository
public interface GameRepo extends JpaRepository<Game, Long> {

}
