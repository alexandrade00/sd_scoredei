package sd.Projeto2.scoreDEI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sd.Projeto2.scoreDEI.models.User;

public interface UserRepo extends JpaRepository<User, Long> {

    public User findByUsername(String username);
    
}
