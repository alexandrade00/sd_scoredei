package sd.Projeto2.scoreDEI.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sd.Projeto2.scoreDEI.models.Team;

@Repository
public interface TeamRepo extends JpaRepository<Team, Long>{

    Team findByTeamName(String teamName);
}
