package sd.Projeto2.scoreDEI.misc;

import sd.Projeto2.scoreDEI.models.Player;
import sd.Projeto2.scoreDEI.models.Team;

public class Statistics {
    private Team team;
    private int numGames;
    private int wins;
    private int losses;
    private int draws;
    private Player bestScorer;
    

    public Statistics() {
    }

    public Statistics(Team team, int numGames, int wins, int losses, int draws, Player bestScorer) {
        this.team = team;
        this.numGames = numGames;
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
        this.bestScorer = bestScorer;
    }

    public Team getTeam() {
        return this.team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public int getNumGames() {
        return this.numGames;
    }

    public void setNumGames(int numGames) {
        this.numGames = numGames;
    }

    public int getWins() {
        return this.wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return this.losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getDraws() {
        return this.draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public Player getBestScorer() {
        return this.bestScorer;
    }

    public void setBestScorer(Player bestScorer) {
        this.bestScorer = bestScorer;
    }

    public void incrementGames() {
        this.numGames++;
    }

    public void incrementWins() {
        this.wins++;
    }

    public void incrementLosses() {
        this.losses++;
    }

    public void incrementDraws() {
        this.draws++;
    }
}
